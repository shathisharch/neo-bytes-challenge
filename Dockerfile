FROM python:3
ADD my_first_script.py /
RUN pip install flask
RUN pip install flask_restful
EXPOSE 3333
CMD [ "python", "./my_first_script.py"]
